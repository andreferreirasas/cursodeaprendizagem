<?php include("../../classes/config.php"); ?>
<?php include("../../classes/DB.class.php"); ?>
<?php include("../../classes/CRUD.class.php"); ?>
<?php include('../../classes/PHPMailerAutoload.php'); ?>

<?php
    echo '<option value=""></option>';
    extract($_POST);
    $sel = CRUD::SelectOne('escolas','id_cidade',$cidade,'escola ASC');
    foreach ($sel['dados'] as $cidades) {
        echo '<option value="'.$cidades['id'].'">'.$cidades['escola'].'</option>';
    }
    echo '<option value="99999999">Outras</option>';
?>
<script>
    $('#local_outra_escola').hide('slow');
    $(document).on('change', '#escola', function (event) {
        event.preventDefault();
        var escola = $('option:selected', this).val();
        if (escola == '99999999') {
            $('#local_outra_escola').show();
            $('#outra_escola').attr('required', 'required');
        } else {
            $('#local_outra_escola').hide();
            $('#outra_escola').removeAttr('required');
        }
    });
</script>