<?php
include('templates/header.php');

if (isset($minha_bandeira) && !empty($minha_bandeira)) {
    die();
}

extract($_GET);
$sel = array();
if (isset($id)) {
    $sel = CRUD::SelectOne('escolas', 'id', $id);
    if ($sel['num'] > 0) {
        $cidade = CRUD::SelectOne('cidades', 'id', $sel['dados'][0]['id_cidade']);
    }
}

if (isset($_POST['salvar'])) {
    $sel['dados'][] = $_POST;
}

//echo '<pre>'; print_r($sel); echo '</pre>';
//echo '<pre>'; print_r($cidade); echo '</pre>';
?>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <?php include('templates/topo.php'); ?>
    <?php include('templates/menu.php'); ?>
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Administradores</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Administradores</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Usuário Administrador</h4>
                            <!-- <h6 class="card-subtitle">Os QRCode são utilizados para trasmitir informações necesárias de forma segura. Este QR Code dará a capacidade de um ou mais alunos se cadastram nas aplicações selecionadas. </h6> -->
                            <form class="m-t-40" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <h5>Escola <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="escola" value="<?php echo isset($sel['dados'][0]['escola']) ? $sel['dados'][0]['escola'] : '' ?>" class="form-control" required data-validation-required-message="Este Campo é Obrigatório">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Estado <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <select class="form-control" id="estado">
                                            <option value=""></option>
                                            <?php 
                                                if (isset($cidade['dados'][0]['estado']) && strlen($cidade['dados'][0]['estado']) > 2) {
                                                    $estados = $estadosJaponeses; 
                                                } else {
                                                    $estados = $estadosBrasileiros;
                                                } 
                                                foreach ($estados as $sigla => $estado) { 
                                                    $selecionado = isset($cidade['dados'][0]['estado']) && $sigla == $cidade['dados'][0]['estado'] ? 'selected="selected"' : '';
                                                    ?><option value="<?php echo $sigla ?>" <?php echo $selecionado ?>><?php echo $estado ?></option><?php                                                 
                                                } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Cidade <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <select class="form-control" id="cidade" name="id_cidade" required data-validation-required-message="Este Campo é Obrigatório">
                                            <option value=""></option>
                                            <?php 
                                                if (isset($cidade['dados'])) {
                                                    ?><option value="<?php echo $cidade['dados'][0]['id'] ?>" selected="selected"><?php echo $cidade['dados'][0]['cidade'] ?></option><?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Endereço</h5>
                                    <div class="controls">
                                        <input type="text" name="endereco" value="<?php echo isset($sel['dados'][0]['endereco']) ? $sel['dados'][0]['endereco'] : '' ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>CEP</h5>
                                    <div class="controls">
                                        <input type="text" name="cep" value="<?php echo isset($sel['dados'][0]['cep']) ? $sel['dados'][0]['cep'] : '' ?>" class="form-control">
                                    </div>
                                </div>                                
                                <div class="text-xs-right">
                                    <button type="submit" name="salvar" class="btn btn-info">Salvar</button>
                                    <a href="<?php echo $url_site_admin . 'lista_usuarios.php' ?>" class="btn btn-inverse btn-danger">Cancelar</a>
                                </div>
                                <?php
                                $erros = '';
                                if (isset($_POST['salvar'])) {

                                    $_POST['cep'] = preg_replace('/[^0-9]/', '', $_POST['cep']);
                                    if (empty($_POST['cep'])) {
                                        unset($_POST['cep']);
                                    }
                                    
                                    $salvar = CRUD::Insert('escolas', $_GET['id']);
                                    
                                    if ($salvar) {
                                        $alerta = 'Sucesso!';
                                        $mensagem = 'Escola salva com sucesso';                                        
                                    } else {
                                        $alerta = 'Falha!';
                                        $mensagem = 'Falha ao salvar escola';          
                                    }
                                    ?>
                                    <div id="modal_mensagem" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel"><?php echo $alerta ?></h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <p><?php echo $mensagem ?></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <?php if ($alerta == 'Sucesso!') { ?>
                                                        <button type="button" class="btn btn-info waves-effect" onclick="window.location.href = './lista_escolas.php'">Fechar</button>
                                                    <?php } else { ?>
                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <?php include('templates/sidebar.php'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    <?php include('templates/footer.php'); ?>

    <script type="text/javascript">
        $(document).ready(function() { 
            //$("#cidade").select2(); 
        });
        
        $(document).on('change', '#pais', function (event) {
            event.preventDefault();
            var pais = $('option:selected', this).val();
            if (pais == 'BR') {
                $('label.estado').text('Estado');
            } else if (pais == 'JP') {
                $('label.estado').text('Província');
            }
            $.ajax({
                type: "post",
                url: "js/ajax/estados.php",
                data: 'pais=' + pais,
                dataType: "html",
                success: function (response) {
                    $('#estado').html(response);
                }
            });
        });

        $(document).on('change', '#estado', function (event) {
            event.preventDefault();
            var estado = $('option:selected', this).val();
            $.ajax({
                type: "post",
                url: "js/ajax/cidades.php",
                data: 'estado=' + estado,
                dataType: "html",
                success: function (response) {
                    $('#cidade').html(response);
                }
            });
        });
    </script>
    
    <script>
        <?php if (isset($mensagem) && !empty($mensagem)) { ?>
            $('#modal_mensagem').modal();
        <?php } ?>
    </script>
    
    <style>
            form .local-senha {
                position: relative !important;
            }
            form .ver-senha {
                /*opacity: 0.4;*/
                color: #6c757d!important;
                font-size: 18px !important;
                cursor: pointer !important;
                position: absolute !important;
                top: 10px !important;
                right: 15px !important;
                z-index: 99 !important;
            }
        </style>
        <script type="text/javascript">
            $(document).on('click','.ver-senha', function (e) {
                if ($(this).hasClass('fa-eye')) {
                    $(this).removeClass('fa-eye')
                    $(this).addClass('fa-eye-slash')
                    $(this).siblings('#senha').attr('type', 'text');
                    $(this).siblings('#senha').focus();
                } else {
                    $(this).removeClass('fa-eye-slash')
                    $(this).addClass('fa-eye')
                    $(this).siblings('#senha').attr('type', 'password');
                    $(this).siblings('#senha').focus();
                }     
            });  
        </script>