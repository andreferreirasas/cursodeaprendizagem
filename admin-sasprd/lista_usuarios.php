<?php
include('templates/header.php');

if (isset($minha_bandeira) && !empty($minha_bandeira)) {
    die();
}
?>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <?php include('templates/topo.php'); ?>
    <?php include('templates/menu.php'); ?>

    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Administradores</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Administradores</li>
                        </ol>
                        <a href="usuario.php" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Criar Novo</a>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Usuários Administradores Cadastrados</h4>
                            <div class="table-responsive m-t-40">
                                <table id="lista_usuarios" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%" data-table="administradores">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>E-mail</th>
                                            <th>Login</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sel = CRUD::Select('administradores');
                                        foreach ($sel['dados'] as $lista) {
                                            // $ativo = ($lista['ativo'] == 0) ? 'Não' : "sim";
                                            ?>
                                            <tr class="item<?php echo $lista['id'] ?>">
                                                <td><?php echo $lista['nome'] ?></td>
                                                <td><?php echo $lista['email'] ?></td>
                                                <td><?php echo $lista['login'] ?></td>
                                                <td style="width: 50px;">
                                                    <a class="btn btn-info btn-circle" href="usuario.php?id=<?php echo $lista['id'] ?>"><i class="fas fa-pencil-alt"></i></a>
                                                    <a class="btn btn-danger btn-circle deleteitem" href="<?php echo $lista['id'] ?>"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <?php include('templates/sidebar.php'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    <?php include('templates/footer.php'); ?>