<?php
include('templates/header.php');

if (isset($minha_bandeira) && !empty($minha_bandeira)) {
    die();
}

if (!isset($_POST['escola']) && isset($_GET['escola'])) {
    $_POST['escola'] = $_GET['escola'];
}
?>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <?php include('templates/topo.php'); ?>
    <?php include('templates/menu.php'); ?>

    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Escolas</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Escolas</li>
                        </ol>
                        <a href="escola.php" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Criar Nova</a>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Escolas Cadastradas</h4>                            
                            <div class="m-t-40">
                                <form method="post" action="lista_escolas.php">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <input type="text" name="escola" class="form-control" required data-validation-required-message="Este Campo é Obrigatório">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" name="pesquisar" class="btn btn-block btn-info">Pesquisar</button>
                                        </div>
                                    </div>
                                </form>
                                <table id="lista_escolas" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%" data-table="escolas">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Estado</th>
                                            <th>Cidade</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $registros = 20;
                                        $pagina = isset($_GET['p']) ? (int)$_GET['p'] : 1;
                                        $inicio = isset($_GET['p']) ? ((int)$_GET['p'] - 1) * $registros : 0;
                                        $condicao = isset($_POST['escola']) ? "WHERE e.escola LIKE '%{$_POST['escola']}%'" : "";
                                        
                                        $sel = CRUD::SelectExtra("SELECT e.id, c.estado, c.cidade, escola, endereco, cep	 
                                                                  FROM escolas AS e LEFT JOIN cidades AS c ON (c.id = e.id_cidade)
                                                                  {$condicao}
                                                                  ORDER BY e.escola 
                                                                  LIMIT {$inicio}, {$registros}");
                                        
                                        foreach ($sel['dados'] as $lista) {
                                            // $ativo = ($lista['ativo'] == 0) ? 'Não' : "sim";
                                            ?>
                                            <tr class="item<?php echo $lista['id'] ?>">
                                                <td><?php echo $lista['escola'] ?></td>
                                                <td><?php echo $lista['estado'] ?></td>
                                                <td><?php echo $lista['cidade'] ?></td>
                                                <td style="width: 50px;">
                                                    <a class="btn btn-info btn-circle" href="escola.php?id=<?php echo $lista['id'] ?>" title="Editar"><i class="fas fa-pencil-alt"></i></a>
                                                    <a class="btn btn-warning btn-circle" href="lista_cursistas.php?id=<?php echo $lista['id'] ?>" title="Cursistas"><i class="fas fa-user"></i></a>
                                                    <!--<a class="btn btn-danger btn-circle deleteitem" href="<?php echo $lista['id'] ?>"><i class="fas fa-trash-alt"></i></a>-->
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php 
                                $atesedepois = 3;
                                $ecolas = CRUD::Select("escolas as e $condicao"); 
                                $total = ceil($ecolas['num'] / $registros);                                
                                $urlescola = isset($_POST['escola']) && !empty($_POST['escola']) ? "&escola={$_POST['escola']}" : "";
                                ?>                                
                                <table width="100%">
                                    <tr>
                                        <td style="padding: 0 0 0 15px;">
                                            <strong><?php echo $ecolas['num'] ?></strong> registros (página <strong><?php echo $pagina ?></strong> de <strong><?php echo $total ?></strong>)
                                        </td>
                                        <td>
                                            <nav aria-label="...">
                                                <ul class="pagination justify-content-end">
                                                    <li class="page-item <?php echo $pagina == 1 ? 'disabled' : '' ?>">
                                                        <a class="page-link" href="?p=1<?php echo $urlescola ?>" tabindex="-1"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a>                                            
                                                    </li>
                                                    <li class="page-item <?php echo $pagina == 1 ? 'disabled' : '' ?>">
                                                        <a class="page-link" href="?p=<?php echo $pagina - 1 ?><?php echo $urlescola ?>" tabindex="-1"><i class="fa fa-angle-left" aria-hidden="true"></i></a>                                            
                                                    </li>
                                                    <?php for ($i = ($pagina - $atesedepois); $i < $pagina; $i++) { ?>
                                                        <?php if ($i > 0) { ?>
                                                            <li class="page-item" style="display: initial">
                                                                <a class="page-link" href="?p=<?php echo $i ?><?php echo $urlescola ?>"><strong><?php echo $i ?></strong></a>
                                                            </li>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <li class="page-item active">
                                                        <a class="page-link" href="?p=<?php echo $pagina ?><?php echo $urlescola ?>"><strong><?php echo $pagina ?></strong><span class="sr-only">(current)</span></a>
                                                    </li>
                                                    <?php for ($i = ($pagina + 1); $i <= ($pagina + $atesedepois); $i++) { ?>
                                                        <?php if ($i <= $total) { ?>
                                                            <li class="page-item" style="display: initial">
                                                                <a class="page-link" href="?p=<?php echo $i ?><?php echo $urlescola ?>"><strong><?php echo $i ?></strong></a>
                                                            </li>
                                                        <?php } ?>
                                                    <?php } ?>      
                                                    <li class="page-item <?php echo $pagina == $total ? 'disabled' : '' ?>">
                                                        <a class="page-link" href="?p=<?php echo $pagina + 1 ?><?php echo $urlescola ?>" tabindex="-1"><i class="fa fa-angle-right" aria-hidden="true"></i></a>                                            
                                                    </li>
                                                    <li class="page-item <?php echo $pagina == $total ? 'disabled' : '' ?>">
                                                        <a class="page-link" href="?p=<?php echo $total ?><?php echo $urlescola ?>" tabindex="-1"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>                                            
                                                    </li>                                        
                                                </ul>
                                            </nav>
                                        </td>
                                    </tr>
                                </table>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <?php include('templates/sidebar.php'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    <?php include('templates/footer.php'); ?>

    <script>
        //$('#lista_escolas').DataTable({ language: traducao, searching: true })
    </script>
        
        