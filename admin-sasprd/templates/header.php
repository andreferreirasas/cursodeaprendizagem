<?php 
/*
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL ^E_NOTICE);
*/
ob_start();
session_start(); 
?>
<?php include("../classes/config.php"); ?>
<?php
if(!isset($_SESSION[SESSION_ADMIN.'_uid'])){
    header('Location: '.$url_site);
}
?>
<?php include("../classes/DB.class.php"); ?>
<?php include('../classes/PHPMailerAutoload.php'); ?>
<?php include("../classes/class.upload.php"); ?>
<?php include("../classes/Geral.class.php"); ?>
<?php include("../classes/CRUD.class.php"); ?>
<?php include("../classes/Dados.class"); ?>

<?php $skin = 'skin-green'; ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.ico">
    <title>Curso de Avaliação de Aprendizagem</title>
    <link href="assets/node_modules/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link href="dist/css/custom.css?v=1.7" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="assets/node_modules/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/node_modules/dropify/dist/css/dropify.min.css">
    <style>
        .user-name {
            padding-top: 7px;
            padding-right: 20px;
            color: #E2E7ED;
        }
        #grafico-dias, rect, path  {
            cursor: pointer;
        }   
        .page-wrapper {
            background-color: #fff;
        } 
        .col-12 .card {
            border: 1px dotted #c2c2c2;
            border-radius: 5px;
        }         
        .page-titles {
            border-bottom: 1px dotted #c2c2c2;
        }
        /*
        @media (min-width:1170px) {
            .navbar-brand b img {
                // width: 120px !important;
                // position:relative; top:10px; left:10px;
            }
            .navbar-brand {
                text-align: center !important;
            }
        }
        */
    </style>
</head>

<body class="<?php echo $skin ?> fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Curso de Avaliação de Aprendizagem</p>
        </div>
    </div>
