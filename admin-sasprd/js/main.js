$(document).ready(function () {

	$(document).on('click', '.deleteitem', function (event) {
		event.preventDefault();
		var table = $(this).parents('table').attr('data-table');
		var item = $(this).attr('href');
		if (confirm("Tem certeza que deseja deletar este item?") == true) {
			$.ajax({
				type: "POST",
				url: "js/ajax/delete.php",
				data: {table: table, item: item},
				success: function (response) {
					$('.item'+item).remove();
				}
			});
		}
	});

	$(document).on('click', '.alterasenha', function (event) {
		event.preventDefault();
		var item = $(this).attr('href');
		$('div#modal_senha #idaluno').val(item);
		$('#modal_senha').modal();
	});

	$(document).on('click', '.confirmaalterasenha', function (event) {
		var usuario = $('#idaluno').val();
		var senha = $('#novasenha').val();
		if (usuario == '' || usuario == undefined) {
			$(".alerta-usuario").css("display", "block");
		} else if (senha.length == 0) {
			$(".alerta-senha-branco").css("display", "block");
		} else if (senha.length > 0 && senha.length < 3) {
			$(".alerta-senha-minimo").css("display", "block");
		} else if (senha.length > 20) {
			$(".alerta-senha-maximo").css("display", "block");
		} else {
			$.ajax({
				type: "POST",
				url: "js/ajax/altera_senha.php",
				data: {usuario: usuario, senha: senha},
				success: function (response) {
					resetSenha();
					$('#modal_senha').modal('hide');
					$('#modal_mensagem_titulo').text('Sucesso!');
					$('#modal_mensagem_texto').text('Senha alterada com sucesso.');
					$('#modal_mensagem').modal();
				},
				error: function (response) {
					resetSenha();
					$('#modal_senha').modal('hide');
					$('#modal_mensagem_titulo').text('Falha!');
					$('#modal_mensagem_texto').text('Não foi possível alterar a senha.');
					$('#modal_mensagem').modal();
				}
			});
		}
	});

	$(document).on('click', '.cancelaalterasenha', function (event) {
		resetSenha();
	});

	function resetSenha() {
		$('#idaluno').val('');
		$('#novasenha').val('');
		$(".versenha").css("display", "block");
		$(".escondersenha").css("display", "none");
		$("#novasenha").attr("type", "password");
	}

	$('#novasenha').keyup(function (event) {
		var senha = $('#novasenha').val();
		if (senha.length > 0) {
			$(".alerta-senha-branco").css("display", "none");
		} 
		if (senha.length >= 3) {
			$(".alerta-senha-minimo").css("display", "none");
		} 
		if (senha.length <= 20) {
			$(".alerta-senha-maximo").css("display", "none");
		} 
	});

	$(document).on('click', '.versenha', function (event) {
		$(".versenha").css("display", "none");
		$(".escondersenha").css("display", "block");
		$("#novasenha").attr("type", "text");
	});

	$(document).on('click', '.escondersenha', function (event) {
		$(".versenha").css("display", "block");
		$(".escondersenha").css("display", "none");
		$("#novasenha").attr("type", "password");
	});

	$(document).on('click', '.registros', function (event) {
		var campo = $(this).data('campo');
		var termo = $(this).data('termo');
		$('#campo_info').val(campo);
		$('#termo_info').val(termo);
		$('form#pesquisa_info').submit();
	});

});