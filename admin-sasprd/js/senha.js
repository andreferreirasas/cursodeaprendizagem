$(document).on('click', '.versenha', function (event) {
    $(".versenha").css("display", "none");
    $(".escondersenha").css("display", "block");
    $("#novasenha").attr("type", "text");
});

$(document).on('click', '.escondersenha', function (event) {
    $(".versenha").css("display", "block");
    $(".escondersenha").css("display", "none");
    $("#novasenha").attr("type", "password");

});

$(document).on('click', '.confirmaalterasenha', function (event) {
    var senha = $('#novasenha').val();
    if (senha.length == 0) {
        $(".alerta-senha-branco").css("display", "block");
    } else if (senha.length > 0 && senha.length < 3) {
        $(".alerta-senha-minimo").css("display", "block");
    } else if (senha.length > 20) {
        $(".alerta-senha-maximo").css("display", "block");
    } else {
        $('form#recuperarsenha').submit();
    }
});

$('#novasenha').keyup(function (event) {
    var senha = $('#novasenha').val();
    if (senha.length > 0) {
        $(".alerta-senha-branco").css("display", "none");
    } 
    if (senha.length >= 3) {
        $(".alerta-senha-minimo").css("display", "none");
    } 
    if (senha.length <= 20) {
        $(".alerta-senha-maximo").css("display", "none");
    } 
});