<?php
session_start();
include('../../../classes/config.php');
include('../../../classes/DB.class.php');
include('../../../classes/CRUD.class.php');
include('../../../classes/class.upload.php');
include('../../../classes/PHPMailerAutoload.php');
include('../../../classes/Geral.class.php');

extract($_POST);

$cadastro = CRUD::SelectOne('administradores','email',$email);

header('Content-Type: application/json');

if ($cadastro['num'] > 0) {
    try {
        $codigo = CRUD::GerarCodigoEmail($cadastro['dados'][0]['id']);
        $link = "www.teste.com/$codigo";
        $assunto = "Redefinir Senha";
        $texto = "Para redefinir sua senha acesso o link: ".$link;
        $para = $cadastro['dados'][0]['email'];
        $nome = $cadastro['dados'][0]['nome'];
        $menssagem = '
            <div style="border:2px solid rgba(252,190,27,1);padding:10px;font-family:Tahoma;font-size:13px;text-align:center;border-radius:4px">
                <div class="adM">
                </div><img src="'.$url_site_admin.'assets/images/logo-eipg.jpg" style="width:80px" class="CToWUd a6T" tabindex="0">
                <div class="a6S" dir="ltr" style="opacity: 0.01;">
                    <div id=":th" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Fazer o download" role="button" tabindex="0" aria-label="Fazer o download do anexo " data-tooltip-class="a1V">
                        <div class="aSK J-J5-Ji aYr"></div>
                    </div>
                </div>
                <p>Olá <b>'.$nome.'</b>, tudo bem?
                    <br>
                    <br>Foi solicitado a redefinição de senha do painel Painel do EI Play Games.
                    <br>
                    <b>Para realizar a alteração de sua senha clique no link abaixo:</b>
                    <br>
                </p>
                <a href="'.$url_site_admin.'recuperar_senha.php?codigo='.$codigo.'" target="_blank" data-saferedirecturl="'.$link.'">Redefinir senha aqui.</a>
                <br>
                <br>
                <p>Atenciosamente,
                    <br>
                    <b>Suporte Congresso Cordimariano</b>
                    <br>
                    <a href="mailto:'.MAIL_SEND.'" target="_blank">suporte.app@<wbr>escoladainteliencia.com.br</a>
                    <br>
                    <br>
                    <small>Esta é uma mensagem automática, por favor, não responda.</small></p>
                <div class="yj6qo"></div>
                <div class="adL">
                </div>
            </div>
        ';        

        $envio = Geral::SendMail($assunto, $texto, $menssagem, $para, $nome);

        if ($envio) {
            $resposta = array (
                'resposta' => 'ok',
                'mensagem' => 'Foi enviado um e-mail para ' . $email . ' com instruções para criação de uma nova senha.',
            );
        } else {
            $resposta = array (
                'resposta' => 'error',
                'mensagem' => 'Falha ao enviar o e-mail.',
            );
        }
        
        echo json_encode($resposta);
    } catch (Exception $e) {
        header("HTTP/1.0 400 Bad Request"); 
        $resposta = array (
            'resposta' => 'error',
            'mensagem' => 'Falha ao enviar o e-mail.',
        );
        echo json_encode($resposta);
    }    
} else {
    $resposta = array (
        'resposta' => 'error',
        'mensagem' => 'E-mail não encontrado.',
    );
    echo json_encode($resposta);
}
?>