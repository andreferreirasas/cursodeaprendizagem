<?php
session_start(); 

include("../../../classes/config.php");
include("../../../classes/DB.class.php");
include('../../../classes/PHPMailerAutoload.php');
include("../../../classes/CRUD.class.php");
include("../../../classes/Dados.class");

if(!isset($_SESSION[SESSION_ADMIN.'_uid'])){
    die();
}
