<?php 
include_once ('./include.php');

$registros_dia = 100;

extract($_POST);
 
if (!isset($data)) {
    $data = '';
}
if (!isset($escola)) {
    $escola = '';
}
if (!isset($perfil) || empty($perfil)) {
    $perfil = 'usuarios';
}
if (!isset($acao) || empty($acao)) {
    $acao = 'incritos';
}

$dados = Dados::incritos($data, $escola, '', $perfil);

$cargos = array(
    'Educador' => 'Educador',
    'Pedagogica' => 'Gestão Pedagógica',
    'Administrativo' => 'Administrativo',
);
?>

<?php if ($perfil == 'usuarios') { ?>
    <table id="lista_dias" class="display table table-hover table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Escola</th>
                <th  class="text-center" width="120px">Data</th>
            </tr>
        </thead>
        <tbody> 
            <?php
            if (count($dados)) {
                $i = 0;
                foreach ($dados['dados'] as $dado) {
                    $i++;
                    ?>
                    <tr>
                        <td>
                            <?php echo $dado['nome'] ?><br/><small><?php echo $dado['email'] ?></small>
                        </td>
                        <td>
                            <?php echo $dado['escola'] ?><?php echo !empty($dado['outra_escola']) ? "Outras ({$dado['outra_escola']})" : "" ?><br/>
                            <small><?php echo $dado['cidade'] ?> - <?php echo $dado['estado'] ?></small>
                        </td>
                        <td class="text-center">
                            <?php echo $dado['dia'] ?><br/><small><?php echo $dado['hora'] ?></small>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>  
        </tbody>
    </table>
<?php } else { ?>
    <table id="lista_dias" class="display table table-hover table-striped escolas" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Escola</th>
                <th class="text-center">Incrições</th>
            </tr>
        </thead>
        <tbody>  
            <?php
            if (count($dados)) {
                foreach ($dados['dados'] as $dado) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $dado['escola'] ?><?php echo !empty($dado['outra_escola']) ? "Outras ({$dado['outra_escola']})" : "" ?><br/>
                            <small><?php echo $dado['cidade'] ?> - <?php echo $dado['estado'] ?></small>
                        </td>
                        <td  class="text-center">
                            <?php echo $dado['total'] ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>  
        </tbody>
    </table>    
<?php } ?>

<script type="text/javascript">
    $('.incritos, .iniciaram, .concluiram').tooltip();
</script>
