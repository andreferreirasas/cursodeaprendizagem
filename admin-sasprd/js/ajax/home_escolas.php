<?php
include_once ('./include.php');

$registros_home = 100;
?>
<table class="table table-hover">
    <tbody>
        <?php         
            $pagina = isset($_POST['pagina']) ? (int)$_POST['pagina'] : 1;
            $escola = isset($_POST['escola']) ? $_POST['escola'] : '';
            $dados_escolas = Dados::escolas($escola, $pagina, $registros_home);
            ?>
            <tr style="font-size: 16px; font-weight: 500;">
                <td>Escola</td>
                <td class="text-center">Incritos</td>
                <td class="text-center"></td>
            </tr>
            <?php

            foreach ($dados_escolas['dados'] as $dado_escola) {  
                ?>
                <tr class="">
                    <td><?php echo $dado_escola['escola'] ?></td>
                    <td class="text-center">
                        <?php echo $dado_escola['total']; ?>
                    </td>
                    <td><i class="fa fa-users usuarios-escola" aria-hidden="true" data-escola="<?php echo $dado_escola['escola'] ?>"></i></td>
                </tr>
                <?php 
            }
        ?>                                            
    </tbody>
</table>
<?php 
$incricoes = Dados::escolas($escola); 
$atesedepois = 3;
$total = ceil($incricoes['num'] / $registros_home);                                
?>     
<nav aria-label="...">
    <ul class="pagination justify-content-end">
        <?php for ($i = 1; $i <= $total; $i++) { ?>
            <li class="page-item escolas <?php echo $i == $pagina ? 'active' : '' ?>" data-pagina="<?php echo $i ?>">
                <a class="page-link" href="#"><strong><?php echo $i ?></strong></a>
            </li>
        <?php } ?>                                       
    </ul>
</nav>
<div id="modal_detalhes_escolas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered modal_detalhes_escolas">
        <div class="modal-content">
            <div class="modal-header">
                <div class="round align-self-center round-success acessos"><i class="icon-people"></i></div>
                <h4 class="modal-title"><strong></strong> - detalhes</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">                 
                <div class="row">
                    <div class="col-md-12 dados"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('.usuarios-escola').on('click', function (e) {        

        $('#form-escolas select option').removeAttr('selected');
        $('#form-escolas select option.inicial').attr('selected', 'selected');
        
        var escola = $(this).data('escola');
        var bandeira = $('#unidade option:selected').val(); 
        $('#form-escolas #bandeira').val(bandeira);
        
        $('#modal_detalhes_escolas .modal-header .modal-title strong').text(escola);
        $('#modal_detalhes_escolas #form-escolas #escola').val(escola);
        
        $.ajax({
            type: "POST",
            url: "js/ajax/dia.php",
            data: {bandeira: bandeira, perfil: 'usuarios', escola: escola},
            success: function (resposta) {
                $('#modal_detalhes_escolas .dados').html(resposta);
                $('#modal_detalhes_escolas').modal();
            }
        });
    });
    
    $('.page-item.escolas').on('click', function (event) {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        event.preventDefault();
        var unidade = $('#unidade option:selected').val();  
        var escola = $('#pesquisar-escola').val();  
        var pagina = $(this).data('pagina');
        $.ajax({
            type: "POST",
            url: "js/ajax/home_escolas.php",
            data: {unidade: unidade, pagina: pagina, escola: escola},
            success: function (resposta) {
                $('.local-escolas').html(resposta);
            }
        }); 
    });
</script>